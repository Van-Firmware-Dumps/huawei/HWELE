version : huawei.wakeup.v.1.2.2.0
committer : zhengwei
date : 2020.09.17
description : 配置文件不加密

version : huawei.wakeup.v.1.2.1.0
committer : zhengwei
date : 2018.11.23
description : atlanta c20 wakeup2.1

version : huawei.wakeup.v.1.0.3
committer : zhengwei
date : 2018.11.23
description : 你好YOYO修改

version : huawei.wakeup.v.1.0.2
committer : guzhengming
date : 2018.06.27
description : wakeup2.0 enhance

version : huawei.wakeup.v.1.0.1
committer : wangcong
date : 2018.05.16
description : p20 wakeup enhance

version : huawei.wakeup.v.1.0.0
committer : huangli
date : 2017.10.14
description : new
